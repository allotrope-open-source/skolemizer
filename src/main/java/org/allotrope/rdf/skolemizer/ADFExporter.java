package org.allotrope.rdf.skolemizer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import org.allotrope.adf.audit.service.ChangeCapture;
import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.allotrope.datashapes.foaf.model.FoafModel;
import org.allotrope.datashapes.foaf.model.Person;
import org.allotrope.datashapes.prov.model.Entity;
import org.allotrope.datashapes.prov.model.ProvenanceModel;
import org.apache.jena.riot.RDFDataMgr;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.DatasetGraph;

public class ADFExporter {

	public static void main(String[] args) {
		ADFExporter app = new ADFExporter();
		AdfService adfService = AdfServiceFactory.create();
		Path path = Paths.get(args[0]);
		try (AdfFile adfFile = adfService.openFile(path)) {
			app.exportTriples(adfFile, new File(args[1]), "adf://dd/default");
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public void exportTriples(AdfFile adfFile, File triplesFile, String graphIRI) throws IOException {
		Objects.requireNonNull(adfFile);
		Objects.requireNonNull(triplesFile);
		Node localSkolemMapGraphIRI = NodeFactory.createURI("adf://dd/skolemMap");
		Node localGraphIRI = NodeFactory.createURI(graphIRI);
		DatasetGraph ds = adfFile.getDataset().asDatasetGraph();
		Graph sourceGraph = null;
		Graph skolemMapGraph = null;
		ChangeCapture audit = null;
		// we cannot save the skolem mapping directly into ADF without creating a change
		// record
		if (adfFile.isAuditTrailActive()) {
			Person person = FoafModel.person("noreply@allotrope.org").description("Allotrope API").build();
			Entity software = ProvenanceModel.entity("http://tools.allotrope.org/Skolemizer").build();
			audit = adfFile.getAuditTrailService().startCuration(person,
					"skolemization graph stored in ADF for export of data description to an external triple store",
					software);
		}
		sourceGraph = Util.openOrCreateNamedGraph(ds, localGraphIRI);
		skolemMapGraph = Util.openOrCreateNamedGraph(ds, localSkolemMapGraphIRI);
		skolemMapGraph.add(new Triple(localSkolemMapGraphIRI, Util.PAV_DERIVED_FROM, localGraphIRI));

		Skolemizer skolemizer = new Skolemizer();
		FileOutputStream out = new FileOutputStream(triplesFile);
		RDFDataMgr.writeTriples(out, skolemizer.skolemize(sourceGraph, skolemMapGraph));
		out.close();
		if (audit != null) {
			audit.commit();
		}
	}

}
