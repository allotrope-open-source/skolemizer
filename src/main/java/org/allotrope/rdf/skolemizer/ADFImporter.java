package org.allotrope.rdf.skolemizer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import org.allotrope.adf.audit.service.ChangeCapture;
import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.allotrope.datashapes.foaf.model.FoafModel;
import org.allotrope.datashapes.foaf.model.Person;
import org.allotrope.datashapes.prov.model.Entity;
import org.allotrope.datashapes.prov.model.ProvenanceModel;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.sparql.core.DatasetGraph;
import com.hp.hpl.jena.sparql.graph.GraphFactory;

public class ADFImporter {

	public static void main(String[] args) {
		ADFImporter app = new ADFImporter();
		AdfService adfService = AdfServiceFactory.create();
		Path path = Paths.get(args[0]);
		try (AdfFile adfFile = adfService.openFile(path)) {
			app.importTriples(adfFile, new File(args[1]), "adf://dd/default");
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	public void importTriples(AdfFile adfFile, File triplesFile, String graphIRI) throws IOException {
		Objects.requireNonNull(adfFile);
		Objects.requireNonNull(triplesFile);
		Node localSkolemMapGraphIRI = NodeFactory.createURI("adf://dd/skolemMap");
		Node localGraphIRI = NodeFactory.createURI(graphIRI != null ? graphIRI : "adf://dd/default");
		DatasetGraph ds = adfFile.getDataset().asDatasetGraph();
		Graph skolemMapGraph = null;
		ChangeCapture audit = null;
		if (adfFile.isAuditTrailActive()) {
			Person person = FoafModel.person("noreply@allotrope.org").description("Allotrope API").build();
			Entity software = ProvenanceModel.entity("http://tools.allotrope.org/Skolemizer").build();
			audit = adfFile.getAuditTrailService().startCuration(person,
					"import of skolemized graph into of data description", software);
		}
		skolemMapGraph = Util.openOrCreateNamedGraph(ds, localSkolemMapGraphIRI);
		skolemMapGraph.add(new Triple(localSkolemMapGraphIRI, Util.PAV_DERIVED_FROM, localGraphIRI));

		Skolemizer skolemizer = new Skolemizer();
		FileInputStream in = new FileInputStream(triplesFile);
		Graph skolemizedGraph = GraphFactory.createGraphMem();
		RDFDataMgr.read(skolemizedGraph, in, Lang.TURTLE);
		in.close();
		Graph targetGraph = Util.openOrCreateNamedGraph(ds,localGraphIRI);
		skolemizer.unskolemize(skolemizedGraph, targetGraph, skolemMapGraph);
		if (audit != null) {
			audit.commit();
		}
	}
}
