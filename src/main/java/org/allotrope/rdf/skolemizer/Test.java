package org.allotrope.rdf.skolemizer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import org.allotrope.adf.model.AdfFile;
import org.allotrope.adf.service.AdfService;
import org.allotrope.adf.service.AdfServiceFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import com.hp.hpl.jena.graph.Graph;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.NodeFactory;
import com.hp.hpl.jena.graph.Triple;

public class Test {
	@SuppressWarnings("resource")
	public static void main(String[] args) {

		AdfService adfService = AdfServiceFactory.create();
		try {
			Path adf1 = Paths.get("adf1.adf");
			if (Files.exists(adf1))
				Files.delete(adf1);
			AdfFile adfFile = adfService.createFile(adf1);
			Graph graph = Util.openOrCreateNamedGraph(adfFile.getDataset().asDatasetGraph(),
					NodeFactory.createURI("adf://dd/test"));
			Node bob = NodeFactory.createURI("http://example.org/Bob");
			Node mary = NodeFactory.createURI("http://example.org/Mary");
			Node hasChild = NodeFactory.createURI("http://example.org/hasChild");
			Node child = NodeFactory.createAnon();
			graph.add(new Triple(bob, hasChild, child));
			graph.add(new Triple(mary, hasChild, child));
			RDFDataMgr.createGraphWriter(Lang.TURTLE).write(new FileOutputStream("adf-dd-before.ttl"), graph, null,
					null, null);
			adfFile.close();
			adfFile = adfService.openFile(adf1, false);
			ADFExporter exporter = new ADFExporter();
			exporter.exportTriples(adfFile, new File("skolemized.ttl"), "adf://dd/test");
			adfFile.close();
			System.out.print("Modify skolemized.ttl, and then press enter to continue");
			new Scanner(System.in).nextLine();

			adfFile = adfService.openFile(adf1, false);
			ADFImporter importer = new ADFImporter();
			importer.importTriples(adfFile, new File("skolemized.ttl"), "adf://dd/test");
			adfFile.close();
			adfFile = adfService.openFile(adf1, true);
			graph = Util.openOrCreateNamedGraph(adfFile.getDataset().asDatasetGraph(),
					NodeFactory.createURI("adf://dd/test"));
			RDFDataMgr.createGraphWriter(Lang.TURTLE).write(new FileOutputStream("adf-dd-after.ttl"), graph, null, null,
					null);
			adfFile.close();
		} catch (IOException e) {
			System.err.println(e);
		}
	}
}
